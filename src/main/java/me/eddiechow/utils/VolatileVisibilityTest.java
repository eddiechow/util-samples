package me.eddiechow.utils;

/**
 * @Description volatile 可见性测试
 * @Author 周迪
 * @Date 2016-09-13 14:59
 */
public class VolatileVisibilityTest extends Thread{
    //不加 volatile 则子线程无法读取 stop的更新值，则会陷入 while死循环
    private volatile boolean stop;

    @Override
    public void run() {
        long i = 0;
        while (!stop){
            i++;
        }
        System.out.println("finish loop , i is "+i);
    }

    public void stopIt(){
        this.stop = true;
    }

    public boolean getStop(){
        return this.stop;
    }

    public static void main(String[] args) throws InterruptedException {
        VolatileVisibilityTest v = new VolatileVisibilityTest();
        v.start();
        Thread.sleep(1000);
        v.stopIt();
        Thread.sleep(2000);
        System.out.println("finish main");
        System.out.println(v.getStop());
    }
}

package me.eddiechow.utils;

/**
 * @description 线程优先级测试
 * @author 周迪
 * @date 2016-10-28 15:41:53
 */
public class ThreadPriorityTest {
    public volatile static int i = 0;
    public static class AddThread extends Thread{
        @Override
        public void run() {
            for (i=0;i<100000000;i++){
                ;
            }
        }
    }
    public static class Low extends Thread{
        static int count = 0;
        @Override
        public void run() {
            while (true){
                synchronized (ThreadPriorityTest.class){
                    count++;
                    if (count > 1000000){
                        System.out.println("Low");
                        break;
                    }
                }
            }
        }
    }
    public static class High extends Thread{
        static int count = 0;

        @Override
        public void run() {
            while (true){
                synchronized (ThreadPriorityTest.class){
                    count++;
                    if (count > 1000000){
                        System.out.println("High");
                        break;
                    }
                }
            }
        }
    }
    public static void main(String[] args) throws InterruptedException {
        /*AddThread at = new AddThread();
        at.start();
        //等待线程结束
        at.join();
        System.out.println(i);*/

        Low low = new Low();
        High high = new High();
        //高优先级的线程 先完成的概率要大一些
        low.setPriority(Thread.MIN_PRIORITY);
        high.setPriority(Thread.MAX_PRIORITY);
        low.start();
        high.start();
    }
}

package me.eddiechow.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Formatter;

/**
 * @Description 各种保留两位小数的写法
 * @Author 周迪
 * @Date 2016-10-09 14:51
 */
public class Fix2Num {
    public static void main(String[] args) {
        System.out.println(format1(1.020012));
        System.out.println(format2(1.00));
        System.out.println(format3(10000.005123));
        System.out.println(format4(10000.005123));
        System.out.println(format5(10000.195));
    }

    //使用BigDecimal
    public static String format1(double value){
        BigDecimal bd = new BigDecimal(value);
        //保留两位小数，四舍五入
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.toString();
    }

    //使用DecimalFormat
    public static String format2(double value){
        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.HALF_UP);
        return df.format(value);
    }

    //使用NumberFormat
    public static String format3(double value){
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(2);
        nf.setRoundingMode(RoundingMode.HALF_UP);
        nf.setGroupingUsed(false);
        return nf.format(value);
    }

    //使用 java.util.Formatter
    public static String format4(double value){
        return new Formatter().format("%.2f",value).toString();
    }

    //使用String.format
    public static String format5(double value){
        return String.format("%.2f",value).toString();
    }
}

package me.eddiechow.utils;

/**
 * @Description String intern 测试 jdk6、jdk7 结果不一致 原因？常量池的相关处理不一样了
 * @Author 周迪
 * @Date 2016-09-19 11:04
 */
public class StringIntern {
    public static void main(String[] args) {
        String s1 = new String("a");
        s1.intern();
        String s2 = "a";
        System.out.println(s1 == s2);
    }
}
